import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

//css
import './App.css';

//components and pages
import NavBar from './components/NavBar';
import Home from './pages/Home';
import Login from './pages/Login'
import Footer from './components/Footer'



const App = () => {

    return (
    
        <Router>
            <NavBar />
            <div className ="container">
                <Route exact path ="/" component={Login} />
                <Route path ="/Home" component={Home} />
            </div>
            <Footer />
        </Router>
        
        );

       
}

export default App;
