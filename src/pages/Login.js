import React from 'react';
import {Redirect} from 'react-router-dom';

import { connect } from 'react-redux';
import { signIn, signOut } from '../actions/index';

//MUI


class GoogleAuth extends React.Component {

    constructor() {
        super();
           this.state = {
           redirect: false
           }
    };
    

        componentDidMount() {
            window.gapi.load('client:auth2', () =>{
                window.gapi.client.init({
                    clientId:'678241884938-4qskr5793raifbq95bq7i83frs99ce0n.apps.googleusercontent.com',
                    scope: 'email'
                }).then(() =>{
                    this.auth = window.gapi.auth2.getAuthInstance();
                    this.onAuthChange(this.auth.isSignedIn.get());
                    this.auth.isSignedIn.listen(this.onAuthChange);
                });
            })
        }

        onAuthChange = isSignedIn =>{

            if (isSignedIn){
                this.props.signIn(this.auth.currentUser.get().getId());
                this.setState({redirect: true})
            }else{
                this.props.signOut();
            }

        }

        onSignIn = () => {
            this.auth.signIn();
        }

        onSignOut = () => {
            this.auth.signOut();
        }

        renderAuthButton(){
            if (this.props.isSignedIn === null){

                return null;

            } else if( this.props.isSignedIn){
                
                return ( <Redirect to={'/Home'} /> );

            }else{

                return(
                        <div className="container">
                            <h5 className="welcom">To begin! please login with your google account</h5>
                              <button onClick ={this.onSignIn} className="login">
                              <i className="fa fa-google-plus" />   
                                 SignIn with Google
                             </button>
                        </div>
                );
            }
        }
    render(){

        return<div>{ this.renderAuthButton() }</div>;
    }
}

const mapStateToProps =(state)=>{  
    return { isSignedIn: state.auth.isSignedIn };
}
export default connect(mapStateToProps, {signIn, signOut}) (GoogleAuth);