import React, { Component } from "react";
import axios from "axios";

//MUI
import Grid from '@material-ui/core/Grid';

//components
import ViewerList from '../components/ViewerList'
import MovieList from '../components/MovieList';


class Home extends Component {
  state = {
    SearchTerm: "",
    movies: [],
    film: [],
  };

  
  // CRUD operations adding film
   addfilm = (item)=> {
    this.setState({film: item})
    console.log(item);
  }

  deleteFilm = id => {
  const deleteItem = this.state.film.filter(p => p.id !== id)
  this.setState({film: deleteItem})
	}


  // When user types, match the value to state
  onInputChange = e => {
    this.setState({ SearchTerm: e.target.value });
  };

  // On submitting the input, grab the API
  onInputSubmit = e => {
    e.preventDefault();

    const movieName = this.state.SearchTerm;
    const KEY = "531eaffcac14a8c431f91d7a77a345e8";

    const searchQuery = `https://api.themoviedb.org/3/search/movie?api_key=${KEY}&language=en-US&query=${movieName}&page=10`;

    axios.get(searchQuery).then(res => {
      this.setState({ movies: res.data.results });
    });
  };

  render() {
    let cardMarkup = this.state.movies ? (
      this.state.movies.map((movi) => <ViewerList movi ={ movi } addfilm={this.addfilm} />)
    ):(
      <p>Looding...</p>
    );
    return (

      <Grid container spacing={2}>
        <Grid item sm={8} xs={12}>
        <form onSubmit={this.onInputSubmit}>
          <div className="input-field">
           <h5 className = "lebel" html="title">Enter a movie name and hit enter</h5>
          <input type ="text"
           name ="title"
           className ="validate"
           onChange = {this.onInputChange}
           placeholder ="Search for a movie name: type a Hint eg... 'Thomas' and press enter"
m           />
          </div>
        </form>
        <div>
          { cardMarkup }
        </div>
        </Grid>

        <Grid item sm={4} xs={12}>
          <h5 className ="front">Movies List</h5>
          <MovieList film ={this.state.film} deleteFilm={this.deleteFilm}/>
                  
      </Grid>
      </Grid>
    );
  }
}

export default Home;