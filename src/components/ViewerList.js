import React, {useState,} from 'react';
import withStyles from '@material-ui/core/styles/withStyles';


//MUI
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import CardActions from '@material-ui/core/CardActions';


//styles
const styles = {

  card: {
    Width: 275,
    marginBottom: 20,
  },

  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
};



const ViewerList = (props) => {

  // Setting state
  const [item, setItem] = useState([]);

  const { classes, movi : { title, release_date, overview } } = props;

 // CRUD operations add
 const addMovie = (movi)=> {
  setItem([...item, movi])
  console.log(movi);
  
}


return (
<div>
<Card className={classes.card}>
      <CardContent>
        <Typography  color="textSecondary" gutterBottom>
        </Typography>
        <Typography variant="h5" component="h2">
          {title}
        </Typography>
        <Typography  color="textSecondary">
          Released date: {release_date}
        </Typography>
        <Typography variant="body2" component="p">

          <br />
          { overview }
        </Typography>
      </CardContent>
      <CardActions>
        <form
        onSubmit={event => {
          event.preventDefault()  
          props.addfilm(item)
        }}
        >
       <Button onClick={()=>addMovie(props.movi)}
        variant="contained" size="small"
        type="submit">Add to view later movies list</Button>
        </form>
      </CardActions>
      </Card>
      
</div>
  
)

  
}

export default  withStyles(styles)(ViewerList);

