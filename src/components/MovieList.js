import React from 'react';
import Button from '@material-ui/core/Button';

const MovieList = ({film, deleteFilm}) => {

 return(
  <table>
  <thead>
    <tr>
      <th className ="title">Title</th>
      <th className ="title">Released Date</th>
      <th className ="title">Actions</th>
    </tr>
  </thead>
  <tbody>
    {
      film.map(muvi => (
        <tr key={muvi.id}>
          <td>{muvi.title}</td>
          <td>{muvi.release_date}</td>
          <td>
          <Button variant="contained"
            onClick={() => deleteFilm(muvi.id)}
               > 
              Remove</Button>
          </td>
        </tr>
      ))
  
    }
  </tbody>
</table>

 )
}



   

export default MovieList;
