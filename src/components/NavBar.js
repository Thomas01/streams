import React from 'react';
import { Link } from 'react-router-dom';
import Logout from '../pages/Logout';

const Navbar = () => {

    return(
      <div className="navbar">
      <Link className = "active" to="/"><i className = "fa fa-fw fa-home"></i>Streamer</Link> 
      <Link to="/"><i className = "fa fa-fw fa-home"></i>Home</Link> 
      <Link to ="#"><i className="fa fa-fw fa-user"></i></Link>
      <span><Logout /></span>
    </div>
      );
    }

export default Navbar;